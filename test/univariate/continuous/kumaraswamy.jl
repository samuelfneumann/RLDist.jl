@testset "Kumaraswamy" begin
    @testset "eltype" begin
        @test eltype(Kumaraswamy()) === Float64
        @test eltype(Kumaraswamy(1f0)) === Float32
        @test eltype(Kumaraswamy{Int}(0, 1)) === Int
    end

    @testset "rand" begin
        d = Kumaraswamy(rand(), rand())

        samples = [rand(d) for _ in 1:10_000]
        @test mean(samples) ≈ mean(d) rtol=5e-2
        @test std(samples) ≈ std(d) rtol=5e-2

        samples = rand(d, 10_000)
        @test mean(samples) ≈ mean(d) rtol=5e-2
        @test std(samples) ≈ std(d) rtol=5e-2

        d = Kumaraswamy{Int}(0, 1)
        @test rand(d) isa Float64
        @test rand(d, 10) isa Vector{Float64}
        @test rand(d, (3, 2)) isa Matrix{Float64}
    end

    @testset "logpdf"
        d = Kumaraswamy(0.2, 0.1)
        @test logpdf(d, 0.991) ≈ 5.929438522219216
        @test logpdf(d, 0.5) ≈ -1.5174869441841654
        @test logpdf(d, 0.501) ≈ 0.2290214022672038
    end

    @testset "pdf"
        scale = 10.0
        ntests = 10
        for i in 1:ntests
            d = Kumaraswamy(rand() * scale, rand() * scale)
            x = rand()
            @test isapprox(pdf(d, x), exp(logpdf(d, x)); rtol=5e-4)
        end
    end
end
